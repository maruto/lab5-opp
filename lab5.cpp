#include <cstdio>
#include "pthread.h"
#include <cstdlib>
#include <cmath>
#include "mpi.h"
#include <iostream>

#define MESSAGE_TAG 1
#define COUNT_TAG 2
#define TASK_COUNT_BUFFER_TAG 3
#define TASK_BUFFER_TAG 4
#define SEND_COUNT 1
#define SEND_COUNT_TO_RECV 2
#define BREAK -1

using namespace std;

const int L = 5000;
const int TaskListSize = 100;
const double PersentToSend = 0.6;

int Rank;
int Size;

pthread_mutex_t mutex;

int taskDone;
int taskCount;
int *tl;

void *ListeningThread(void *pVoid) {
    while (true) {
        int message;
        MPI_Status status;
        MPI_Recv(&message, 1, MPI_INT, MPI_ANY_SOURCE, MESSAGE_TAG, MPI_COMM_WORLD, &status);
        int sender = status.MPI_SOURCE;
        if (message == BREAK) {
            break;
        }
        if (message == SEND_COUNT) {
            pthread_mutex_lock(&mutex);
            int tasksLeft = taskCount - taskDone;
            pthread_mutex_unlock(&mutex);
            MPI_Send(&tasksLeft, 1, MPI_INT, sender, COUNT_TAG, MPI_COMM_WORLD);
        }
        if (message == SEND_COUNT_TO_RECV) {
            pthread_mutex_lock(&mutex);
            int tasksToSend = (int) ((taskCount - taskDone) * PersentToSend);
            MPI_Send(&tasksToSend, 1, MPI_INT, sender, TASK_COUNT_BUFFER_TAG, MPI_COMM_WORLD);
            if (tasksToSend == 0) {
                pthread_mutex_unlock(&mutex);
                continue;
            }
            MPI_Send(&tl[taskCount - tasksToSend], tasksToSend, MPI_INT, sender, TASK_BUFFER_TAG, MPI_COMM_WORLD);
            taskCount = taskCount - tasksToSend;
            pthread_mutex_unlock(&mutex);
        }
    }
    return NULL;
}

int main(int argc, char **argv) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided != MPI_THREAD_MULTIPLE) {
        cout << "Initialization is not MPI_THREAD_MULTIPLE" << endl;
        MPI_Finalize();
        return 1;
    }
    MPI_Comm_rank(MPI_COMM_WORLD, &Rank);
    MPI_Comm_size(MPI_COMM_WORLD, &Size);

    if (argc != 2) {
        cout << "Usage: " << argv[0] << " listSize" << endl;
        return 1;
    }
    int listSize = atoi(argv[1]);

    pthread_mutex_init(&mutex, NULL);

    pthread_t listeningThread;
    pthread_attr_t attr;
    if (0 != pthread_attr_init(&attr)) {
        perror("Cannot initialize attributes");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    if (0 != pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) {
        perror("Error in setting attributes");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    if (0 != pthread_create(&listeningThread, &attr, ListeningThread, NULL)) {
        perror("Cannot create thread");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    //algorithm
    double globalRes = 0;
    int *countPreProc = new int[Size];
    pthread_mutex_lock(&mutex);
    tl = new int[TaskListSize];
    pthread_mutex_unlock(&mutex);
    for (int iterCount = 0; iterCount < listSize; ++iterCount) {
        //task initialization
        for (int i = 0; i < TaskListSize; ++i) {
            tl[i] = abs(50 - i) * abs(Rank - (iterCount % Size)) * L;
        }
        clock_t begin;
        clock_t end;
        begin = clock();
        //doing tasks

        int globalTaskDone = 0;
        taskDone = 0;
        taskCount = TaskListSize;
        MPI_Barrier(MPI_COMM_WORLD);
        while (true) {
            pthread_mutex_lock(&mutex);
            while (taskDone < taskCount) {
                int task = tl[taskDone];
                //cerr << Rank << " " << tl[taskDone] << " " << taskDone << endl;
                pthread_mutex_unlock(&mutex);
                for (int i = 0; i < task; ++i) {
                    globalRes += sin(i);
                }
                ++globalTaskDone;
                pthread_mutex_lock(&mutex);
                taskDone++;
            }
            pthread_mutex_unlock(&mutex);
            for (int i = 0; i < Size; ++i) {
                int message = SEND_COUNT;
                if (i != Rank) {
                    MPI_Send(&message, 1, MPI_INT, i, MESSAGE_TAG, MPI_COMM_WORLD);
                }
            }
            int count;
            int maxCount = 0;
            int maxCountRank = -1;
            for (int i = 0; i < Size; ++i) {
                if (i != Rank) {
                    MPI_Recv(&count, 1, MPI_INT, i, COUNT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    if (maxCount < count) {
                        maxCount = count;
                        maxCountRank = i;
                    }
                }
            }
            if (maxCountRank == -1) {
                break;
            }
            int message = SEND_COUNT_TO_RECV;
            MPI_Send(&message, 1, MPI_INT, maxCountRank, MESSAGE_TAG, MPI_COMM_WORLD);
            MPI_Recv(&count, 1, MPI_INT, maxCountRank, TASK_COUNT_BUFFER_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            if (count == 0) {
                continue;
            }
            pthread_mutex_lock(&mutex);
            MPI_Recv(tl, count, MPI_INT, maxCountRank, TASK_BUFFER_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            pthread_mutex_unlock(&mutex);
            taskCount = count;
            taskDone = 0;
        }
        end = clock();
        double time = (double) (end - begin) / CLOCKS_PER_SEC;
        for (int i = 0; i < Size; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            if (Rank == i) {
//                cout << "rank " << Rank << ": task done " << globalTaskDone << " in " << iterCount << " iteration"
//                     << endl;
//                cout << "rank " << Rank << ": global res " << globalRes << " in " << iterCount << " iteration"
//                     << endl;
//                cout << "rank " << Rank << ": time " << time << " in " << iterCount << " iteration" << endl;
                cout << time << endl;
		cerr << globalRes << endl;
            }
        }
        double maxTime;
        double minTime;
        MPI_Reduce(&time, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
        MPI_Reduce(&time, &minTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
        if (Rank == 0) {
            cout << "disbalance is " << maxTime - minTime << " in " << iterCount << " iteration" << endl;
            cout << "disbalance part is " << (maxTime - minTime) / maxTime * 100 << "% in " << iterCount << " iteration"
                 << endl;
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
    delete[] tl;
    delete[] countPreProc;
    int message = BREAK;
    MPI_Send(&message, 1, MPI_INT, Rank, MESSAGE_TAG, MPI_COMM_WORLD);
    pthread_join(listeningThread, NULL);
    MPI_Finalize();
    return 0;
}

